package com.spring.boot.design.patterns.creational_design_pattern.builder.enums;

public enum OrderType {
    ON_SITE, TAKE_AWAY;
}
