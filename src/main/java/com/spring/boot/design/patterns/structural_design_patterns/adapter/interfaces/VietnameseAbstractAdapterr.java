package com.spring.boot.design.patterns.structural_design_patterns.adapter.interfaces;

public interface VietnameseAbstractAdapterr {
    void send(String words);
}
