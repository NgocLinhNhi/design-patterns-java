package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.interfaces_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.data.Book;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.data_extends.BusinessBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.data_extends.ProgramingBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.single_dispatch_interfaces.Customer;

public class Developer implements Customer {

    //Phai dung instance of thi moi lay ra dc method dung cua ProgramingBook
    @Override
    public void buy(Book book) {
        if (book instanceof ProgramingBook) {
            ProgramingBook programingBook = (ProgramingBook) book;
            buy(programingBook);
        } else if (book instanceof BusinessBook) {
            BusinessBook businessBook = (BusinessBook) book;
            buy(businessBook);
        } else {
            System.out.println("Developer buy a Book");
        }
    }

    @Override
    public void buy(ProgramingBook book) {
        System.out.println("Developer buy a Programing Book");

    }

    @Override
    public void buy(BusinessBook book) {
        System.out.println("Developer buy a Business Book");
    }
}
