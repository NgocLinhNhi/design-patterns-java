package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.concrete_factory;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Chair;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Table;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.FurnitureAbstractFactory;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces_impl.PlasticChair;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces_impl.PlasticTable;

public class FlasticFactory extends FurnitureAbstractFactory {

    @Override
    public Chair createChair() {
        return new PlasticChair();
    }

    @Override
    public Table createTable() {
        return new PlasticTable();
    }
}
