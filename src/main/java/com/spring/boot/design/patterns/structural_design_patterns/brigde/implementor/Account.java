package com.spring.boot.design.patterns.structural_design_patterns.brigde.implementor;

public interface Account {
    void handleAccount();
}
