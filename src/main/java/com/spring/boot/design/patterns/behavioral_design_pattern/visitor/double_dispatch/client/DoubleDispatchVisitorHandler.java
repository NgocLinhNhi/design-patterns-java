package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.client;

import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element.Book;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl.BusinessBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl.DesignPatternBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl.JavaCoreBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.visitor_interfaces.Visitor;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.visitor_impl.VisitorImpl;

public class DoubleDispatchVisitorHandler {
    public static void main(String[] args) {
        Book book1 = new BusinessBook();
        Book book2 = new JavaCoreBook();
        Book book3 = new DesignPatternBook();


        Visitor v = new VisitorImpl();
        book1.accept(v);
        book2.accept(v);
        book3.accept(v);

        book2.getResource(v);

    }
}
