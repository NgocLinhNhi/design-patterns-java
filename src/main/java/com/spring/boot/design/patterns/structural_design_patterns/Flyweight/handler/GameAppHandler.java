package com.spring.boot.design.patterns.structural_design_patterns.Flyweight.handler;

import com.spring.boot.design.patterns.structural_design_patterns.Flyweight.flyweight_factory.SoldierFactory;
import com.spring.boot.design.patterns.structural_design_patterns.Flyweight.flyweight_interfaces.SoliderService;
import com.spring.boot.design.patterns.structural_design_patterns.Flyweight.parameters.Context;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class GameAppHandler {
    private static final List<SoliderService> soldiers = new ArrayList<>();

    public static void main(String[] args) {
        createSoldier(5, "Viet", 1);
        createSoldier(5, "Ninh", 1);
        createSoldier(3, "Nhi", 3);
        createSoldier(2, "Yuri", 2);
        System.out.println("==================");
        System.out.println("Total soldiers made : " + soldiers.size());
        System.out.println("Total type of soldiers made : " + SoldierFactory.getTotalOfSoldiers());
    }

    private static void createSoldier(int numberOfSoldier, String soldierName, int numberOfStar) {
        for (int i = 1; i <= numberOfSoldier; i++) {
            Context context = new Context("Soldier" + (soldiers.size() + 1), numberOfStar);
            SoliderService soldier = SoldierFactory.createSoldier(soldierName);
            soldier.promote(context);
            soldiers.add(soldier);
        }
    }
}
