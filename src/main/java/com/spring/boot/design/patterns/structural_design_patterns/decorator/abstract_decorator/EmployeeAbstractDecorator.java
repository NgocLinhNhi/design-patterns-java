package com.spring.boot.design.patterns.structural_design_patterns.decorator.abstract_decorator;

import com.spring.boot.design.patterns.structural_design_patterns.decorator.interface_component.EmployeeComponent;

import java.util.Date;

public abstract class EmployeeAbstractDecorator implements EmployeeComponent {

    protected EmployeeComponent employee;

    protected EmployeeAbstractDecorator(EmployeeComponent employee) {
        this.employee = employee;
    }

    //Override method của Interfaces => để class extends abstract => không phải override
    @Override
    public String getName() {
        //Gọi ra xử lý getName() của Interfaces - EmployeeComponentImpl
        return employee.getName();
    }

    @Override
    public void join(Date joinDate) {
        employee.join(joinDate);
    }

    @Override
    public void terminate(Date terminateDate) {
        employee.terminate(terminateDate);
    }
}
