package com.spring.boot.design.patterns.structural_design_patterns.facade.handler;

import com.spring.boot.design.patterns.structural_design_patterns.facade.facade.ShopFacade;

public class FacadeHandler {

    public static void main(String[] args) {
        ShopFacade.getInstance().buyProductByCashWithFreeShipping("contact@gpcoder.com");
        ShopFacade.getInstance().buyProductByPaypalWithStandardShipping("gpcodervn@gmail.com", "0988.999.999");
    }
}
