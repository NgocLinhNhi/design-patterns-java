package com.spring.boot.design.patterns.behavioral_design_pattern.observer.subject;

import com.spring.boot.design.patterns.behavioral_design_pattern.observer.observer.Observer;

public interface Subject {
    void attach(Observer observer);

    void detach(Observer observer);

    void notifyAllObserver();
}
