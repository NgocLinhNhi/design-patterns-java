package com.spring.boot.design.patterns.creational_design_pattern.singleton;

public class Singleton {
    private static Singleton INSTANCE;

    public static Singleton getInstance() {
        if (INSTANCE == null) INSTANCE = new Singleton();
        return INSTANCE;
    }

    private Singleton() {
    }

    public void getCustomer() {
        System.out.println("This is super man");
    }
}
