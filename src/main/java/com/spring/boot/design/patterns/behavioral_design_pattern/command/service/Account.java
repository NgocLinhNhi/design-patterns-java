package com.spring.boot.design.patterns.behavioral_design_pattern.command.service;

public class Account {

    private static Account INSTANCE;

    public static Account getInstance() {
        if (INSTANCE == null) INSTANCE = new Account();
        return INSTANCE;
    }

    public Account() {
    }

    public void open() {
        System.out.println("Account Opened Action");
    }

    public void close() {
        System.out.println("Account  Closed Action");
    }
}
