package com.spring.boot.design.patterns.structural_design_patterns.composite.leaf;

import com.spring.boot.design.patterns.structural_design_patterns.composite.base_component.FileComponent;

public class FileLeaf implements FileComponent {
    private String name;
    private long size;

    public FileLeaf(String name, long size) {
        super();
        this.name = name;
        this.size = size;
    }

    @Override
    public long totalSize() {
        return size;
    }

    @Override
    public void showProperty() {
        System.out.println("FileLeaf [name=" + name + ", size=" + size + "]");
    }
}
