package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces;

public interface Chair {

    /**
     *
     * interface cho thực thi các nghiệp vụ chính cho các method của abstract Class
     *
     */
    void create();
}
