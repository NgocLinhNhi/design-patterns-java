package com.spring.boot.design.patterns.structural_design_patterns.decorator.concrete_component;

import com.spring.boot.design.patterns.structural_design_patterns.decorator.interface_component.EmployeeComponent;
import com.spring.boot.design.patterns.structural_design_patterns.decorator.util.DateUtil;

import java.util.Date;

public class EmployeeComponentImpl implements EmployeeComponent {
    private final String name;

    public EmployeeComponentImpl(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void join(Date joinDate) {
        System.out.println(this.getName() + " joined on " + DateUtil.formatDate(joinDate));
    }

    @Override
    public void terminate(Date terminateDate) {
        System.out.println(this.getName() + " terminated on " + DateUtil.formatDate(terminateDate));
    }

    @Override
    public void doTask() {
        // Unassigned task
    }
}
