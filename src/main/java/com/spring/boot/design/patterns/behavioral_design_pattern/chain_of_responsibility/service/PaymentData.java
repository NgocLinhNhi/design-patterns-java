package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class PaymentData {

    //Nên để 1 class làm data để handle không nên dùng 1 class entity làm class data ( quá gò bó )
    private final Map<String, Object> stepDataMap;
    private final int customerId;
    private final BigDecimal amount;
    private long startStep1ProcessTime;
    private long step2ProcessTime;

    PaymentData(int customerId,
                BigDecimal amount) {
        this.stepDataMap = new HashMap<>();
        this.customerId = customerId;
        this.amount = amount;
    }

    //Data transfer for each steps
    public void addStepData(String key, Object data) {
        this.stepDataMap.put(key, data);
    }

    @SuppressWarnings("unchecked")
    public <T> T getStepData(String key) {
        Object data = stepDataMap.get(key);
        if (data == null)
            throw new IllegalArgumentException("has no step data with key: " + key + ", it may has removed");
        return (T) data;
    }

    //=========================== getter / setter ==================

    public int getCustomerId() {
        return customerId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public long getStep2ProcessTime() {
        return step2ProcessTime;
    }

    public void setStep2ProcessTime(long step2ProcessTime) {
        this.step2ProcessTime = step2ProcessTime;
    }

    public long getStartStep1ProcessTime() {
        return startStep1ProcessTime;
    }

    public void setStartStep1ProcessTime(long startStep1ProcessTime) {
        this.startStep1ProcessTime = startStep1ProcessTime;
    }
}
