package com.spring.boot.design.patterns.behavioral_design_pattern.command.command_interfaces;

public interface Command {
    //chính là các command xử lý sql trong DB
    void execute();
}
