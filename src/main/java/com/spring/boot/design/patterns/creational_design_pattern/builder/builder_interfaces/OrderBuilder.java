package com.spring.boot.design.patterns.creational_design_pattern.builder.builder_interfaces;

import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.BreadType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.OrderType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.SauceType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.VegetableType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.product.Order;
import com.spring.boot.design.patterns.creational_design_pattern.builder.request.ProductRequest;

public interface OrderBuilder {
    OrderBuilder setOrderType(OrderType orderType);

    OrderBuilder setOrderBread(BreadType breadType);

    OrderBuilder setOrderSauce(SauceType sauceType);

    OrderBuilder setOrderVegetable(VegetableType vegetableType);

    OrderBuilder setProduct(ProductRequest productRequest);

    Order build();
}
