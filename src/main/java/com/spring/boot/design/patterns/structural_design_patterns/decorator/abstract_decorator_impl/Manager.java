package com.spring.boot.design.patterns.structural_design_patterns.decorator.abstract_decorator_impl;

import com.spring.boot.design.patterns.structural_design_patterns.decorator.abstract_decorator.EmployeeAbstractDecorator;
import com.spring.boot.design.patterns.structural_design_patterns.decorator.interface_component.EmployeeComponent;

public class Manager extends EmployeeAbstractDecorator {
    //khởi tạo truyền vào interface
    public Manager(EmployeeComponent employee) {
        super(employee);
    }

    public void createRequirement() {
        System.out.println(this.employee.getName() + " is create requirements.");
    }

    public void assignTask() {
        System.out.println(this.employee.getName() + " is assigning tasks.");
    }

    public void manageProgress() {
        System.out.println(this.employee.getName() + " is managing the progress.");
    }

    //wrap lại nghiệp vụ của  method doTask() => và thêm các action method tương ứng với role .
    @Override
    public void doTask() {
        employee.doTask();
        createRequirement();
        assignTask();
        manageProgress();
    }
}
