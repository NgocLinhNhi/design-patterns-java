package com.spring.boot.design.patterns.structural_design_patterns.facade.subsystems;

public class AccountService {
    public void getAccount(String email) {
        System.out.println("Getting the account of " + email);
    }
}
