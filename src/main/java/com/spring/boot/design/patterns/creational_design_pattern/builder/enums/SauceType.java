package com.spring.boot.design.patterns.creational_design_pattern.builder.enums;

public enum SauceType {
    SOY_SAUCE, FISH_SAUCE, OLIVE_OIL, KETCHUP, MUSTARD;
}
