package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.enums;

import java.util.Map;

public enum ServiceType  {
    EXCHANGE,
    LEVERAGE;
}
