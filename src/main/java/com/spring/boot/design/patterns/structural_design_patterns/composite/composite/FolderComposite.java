package com.spring.boot.design.patterns.structural_design_patterns.composite.composite;

import com.spring.boot.design.patterns.structural_design_patterns.composite.base_component.FileComponent;

import java.util.List;

public class FolderComposite implements FileComponent {
    private List<FileComponent> files;

    public FolderComposite(List<FileComponent> files) {
        this.files = files;
    }

    @Override
    public void showProperty() {
        for (FileComponent file : files) {
            file.showProperty();
        }
    }

    @Override
    public long totalSize() {
        long total = 0;
        for (FileComponent file : files) {
            total += file.totalSize();
        }
        return total;
    }
}
