package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service.PaymentData;

public interface PaymentHandler {

    Object handle(PaymentData data) throws Exception;

}
