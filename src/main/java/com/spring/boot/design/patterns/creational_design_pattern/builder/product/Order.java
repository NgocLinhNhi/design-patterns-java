package com.spring.boot.design.patterns.creational_design_pattern.builder.product;

import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.BreadType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.OrderType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.SauceType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.VegetableType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.request.ProductRequest;

public class Order {
    private OrderType orderType;
    private BreadType breadType;
    private SauceType sauceType;
    private VegetableType vegetableType;
    private ProductRequest productRequest;

    public Order(OrderType orderType,
                 BreadType breadType,
                 SauceType sauceType,
                 VegetableType vegetableType,
                 ProductRequest productRequest) {
        super();
        this.orderType = orderType;
        this.breadType = breadType;
        this.sauceType = sauceType;
        this.vegetableType = vegetableType;
        this.productRequest = productRequest;
    }

    @Override
    public String toString() {
        return "Order Information [setOrderType=" + orderType + ", breadType=" + breadType + ", sauceType=" + sauceType
                + ", vegetableType=" + vegetableType + ", Product Name=" + productRequest.getProuctName() + "]";
    }

    // Get từng request riêng biệt
    public OrderType getOrderType() {
        return orderType;
    }

    public BreadType getBreadType() {
        return breadType;
    }

    public SauceType getSauceType() {
        return sauceType;
    }

    public VegetableType getVegetableType() {
        return vegetableType;
    }

}
