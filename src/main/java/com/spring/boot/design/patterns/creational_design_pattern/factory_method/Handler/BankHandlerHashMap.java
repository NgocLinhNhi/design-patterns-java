package com.spring.boot.design.patterns.creational_design_pattern.factory_method.Handler;

import com.spring.boot.design.patterns.creational_design_pattern.factory_method.enums.BankTypeEnum;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory.BankFactoryByHashMap;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.interfaces.Bank;

public class BankHandlerHashMap {
    public static void main(String[] args) {
        Bank bank = BankFactoryByHashMap.getInstance().getBank(BankTypeEnum.VPBANK);
        System.out.println(bank.getBankName());
        System.out.println("Your Balance in account :" + bank.getAmount());
    }
}
