package com.spring.boot.design.patterns.structural_design_patterns.decorator.abstract_decorator_impl;

import com.spring.boot.design.patterns.structural_design_patterns.decorator.abstract_decorator.EmployeeAbstractDecorator;
import com.spring.boot.design.patterns.structural_design_patterns.decorator.interface_component.EmployeeComponent;

public class TeamLeader extends EmployeeAbstractDecorator {
    public TeamLeader(EmployeeComponent employee) {
        super(employee);
    }

    public void planing() {
        System.out.println(this.employee.getName() + " is planing.");
    }

    public void motivate() {
        System.out.println(this.employee.getName() + " is motivating his members.");
    }

    public void monitor() {
        System.out.println(this.employee.getName() + " is monitoring his members.");
    }

    //wrap lại nghiệp vụ của  method doTask() => và thêm các action method tương ứng với role Team-leader.
    @Override
    public void doTask() {
        employee.doTask();
        planing();
        motivate();
        monitor();
    }
}
