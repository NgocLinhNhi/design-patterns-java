package com.spring.boot.design.patterns.behavioral_design_pattern.iterator.iterator;

public interface ItemIterator<T> {
    boolean hasNext();

    T next();
}
