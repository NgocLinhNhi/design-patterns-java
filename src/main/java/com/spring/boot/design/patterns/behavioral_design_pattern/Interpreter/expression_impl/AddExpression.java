package com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.expression_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.context.InterpreterEngineContext;
import com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.expression.Expression;

public class AddExpression implements Expression {
    private String expression;

    public AddExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public int interpret(InterpreterEngineContext context) {
        return context.add(expression);
    }
}
