package com.spring.boot.design.patterns.creational_design_pattern.builder.concrete_builder;

import com.spring.boot.design.patterns.creational_design_pattern.builder.builder_interfaces.OrderBuilder;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.BreadType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.OrderType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.SauceType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.VegetableType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.product.Order;
import com.spring.boot.design.patterns.creational_design_pattern.builder.request.ProductRequest;

public class FastFoodOrderBuilder implements OrderBuilder {
    private OrderType orderType;
    private BreadType breadType;
    private SauceType sauceType;
    private VegetableType vegetableType;
    private ProductRequest productRequest;

    @Override
    public OrderBuilder setOrderType(OrderType orderType) {
        this.orderType = orderType;
        return this;
    }

    @Override
    public OrderBuilder setOrderBread(BreadType breadType) {
        this.breadType = breadType;
        return this;
    }

    @Override
    public OrderBuilder setOrderSauce(SauceType sauceType) {
        this.sauceType = sauceType;
        return this;
    }

    @Override
    public OrderBuilder setOrderVegetable(VegetableType vegetableType) {
        this.vegetableType = vegetableType;
        return this;
    }

    @Override
    public OrderBuilder setProduct(ProductRequest productRequest) {
        this.productRequest = productRequest;
        return this;
    }

    @Override
    public Order build() {
        return new Order(orderType, breadType, sauceType, vegetableType, productRequest);
    }
}
