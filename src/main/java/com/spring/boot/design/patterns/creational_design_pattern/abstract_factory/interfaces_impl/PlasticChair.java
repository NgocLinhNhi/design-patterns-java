package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces_impl;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Chair;

/**
 *
 * Class mô tả thực thi nghiệp vụ chính được giấu ở đây và được gọi ở các method abstract của - abstractFactory
 *
 */
public class PlasticChair implements Chair {
    @Override
    public void create() {
        System.out.println("Create plastic chair");
    }
}
