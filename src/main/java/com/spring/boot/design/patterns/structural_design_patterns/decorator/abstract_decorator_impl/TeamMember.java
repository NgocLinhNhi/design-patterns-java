package com.spring.boot.design.patterns.structural_design_patterns.decorator.abstract_decorator_impl;

import com.spring.boot.design.patterns.structural_design_patterns.decorator.abstract_decorator.EmployeeAbstractDecorator;
import com.spring.boot.design.patterns.structural_design_patterns.decorator.interface_component.EmployeeComponent;

public class TeamMember extends EmployeeAbstractDecorator {

    //khởi tạo truyền vào interface
    protected TeamMember(EmployeeComponent employee) {
        super(employee);
    }

    public void reportTask() {
        System.out.println(this.employee.getName() + " is reporting his assigned tasks.");
    }

    public void coordinateWithOthers() {
        System.out.println(this.employee.getName() + " is coordinating with other members of his team.");
    }

    //wrap lại nghiệp vụ của  method doTask() => và thêm các action method tương ứng với role .
    @Override
    public void doTask() {
        employee.doTask();
        reportTask();
        coordinateWithOthers();
    }
}
