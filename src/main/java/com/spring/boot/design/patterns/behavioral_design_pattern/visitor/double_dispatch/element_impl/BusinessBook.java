package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element.Book;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.visitor_interfaces.Visitor;

public class BusinessBook implements Book {
    public void accept(Visitor v) {
        v.visit(this);
    }

    @Override
    public void getResource(Visitor v) {
        v.getResource(this);
    }

    public String getPublisher() {
        return "The publisher of business book";
    }

    public String getResource() {
        return "https://github.com/gpcodervn/Design-Pattern-Tutorial/";
    }
}
