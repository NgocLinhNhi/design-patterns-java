package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.handler;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Chair;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Table;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.enums.MaterialType;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.FurnitureAbstractFactory;

public class FurnitureAbstractFactoryHandlerByEnums {
    public static void main(String[] args) {

        FurnitureFactoryByEnums instance = FurnitureFactoryByEnums.getInstance();

        FurnitureAbstractFactory factory = instance.getFactory(MaterialType.FLASTIC);
        Chair chair = factory.createChair();
        chair.create();
        Table table = factory.createTable();
        table.create();

        FurnitureAbstractFactory factory1 = instance.getFactory(MaterialType.WOOD);
        Chair chair1 = factory1.createChair();
        chair1.create();
        Table table1 = factory1.createTable();
        table1.create();
    }
}
