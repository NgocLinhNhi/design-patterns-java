package com.spring.boot.design.patterns.structural_design_patterns.Flyweight.flyweight_impl;

import com.spring.boot.design.patterns.structural_design_patterns.Flyweight.flyweight_interfaces.SoliderService;
import com.spring.boot.design.patterns.structural_design_patterns.Flyweight.parameters.Context;

public class Soldier implements SoliderService {
    private final String name;

    public Soldier(String name) {
        this.name = name;
        System.out.println("Soldier is created! - " + name);
    }

    @Override
    public void promote(Context context) {
        System.out.println(name + " " + context.getId() + " promoted " + context.getStar());
    }

}
