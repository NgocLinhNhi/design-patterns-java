package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.enums.ServiceType;

import java.math.BigDecimal;

public interface FiatDepositService {

    void deposit(int customerId, BigDecimal amount, ServiceType serviceType) throws Exception;
}
