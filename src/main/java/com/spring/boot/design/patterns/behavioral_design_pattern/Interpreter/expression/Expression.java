package com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.expression;

import com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.context.InterpreterEngineContext;

public interface Expression {
    int interpret(InterpreterEngineContext context);
}
