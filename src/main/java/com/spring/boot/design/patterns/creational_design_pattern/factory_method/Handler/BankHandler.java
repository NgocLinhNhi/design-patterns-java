package com.spring.boot.design.patterns.creational_design_pattern.factory_method.Handler;

import com.spring.boot.design.patterns.creational_design_pattern.factory_method.enums.BankTypeEnum;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory.BankFactoryByEnum;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.interfaces.Bank;

public class BankHandler {
    public static void main(String[] args) {
        Bank bank = BankFactoryByEnum.getInstance().getBank(BankTypeEnum.VPBANK);
        System.out.println(bank.getBankName());
    }
}
