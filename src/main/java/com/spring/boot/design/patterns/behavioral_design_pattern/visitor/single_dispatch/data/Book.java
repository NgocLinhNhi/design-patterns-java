package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.data;

import lombok.Data;

@Data
public class Book {
    private String name;
    private int price;
}
