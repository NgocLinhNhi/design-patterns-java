package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.visitor_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl.BusinessBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl.DesignPatternBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl.JavaCoreBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.visitor_interfaces.Visitor;

public class VisitorImpl implements Visitor {
    @Override
    public void visit(BusinessBook book) {
        System.out.println(book.getPublisher());
    }

    @Override
    public void visit(DesignPatternBook book) {
        System.out.println(book.getBestSeller());
    }

    @Override
    public void visit(JavaCoreBook book) {
        System.out.println(book.getFavouriteBook());
    }

    @Override
    public void getResource(BusinessBook book) {
        System.out.println(book.getResource());
    }

    @Override
    public void getResource(DesignPatternBook book) {
        System.out.println(book.getResource());
    }

    @Override
    public void getResource(JavaCoreBook book) {
        System.out.println(book.getResource());
    }
}
