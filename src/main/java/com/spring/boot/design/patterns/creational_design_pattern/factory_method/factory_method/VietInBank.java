package com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory_method;

import com.spring.boot.design.patterns.creational_design_pattern.factory_method.interfaces.Bank;

import java.math.BigDecimal;

public class VietInBank implements Bank {
    @Override
    public String getBankName() {
        return "VietInBank";
    }

    @Override
    public BigDecimal getAmount() {
        return null;
    }
}
