package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element.ProgramingBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.visitor_interfaces.Visitor;

public class DesignPatternBook implements ProgramingBook {
    @Override
    public void accept(Visitor v) {
        v.visit(this);
    }

    @Override
    public void getResource(Visitor v) {
        v.getResource(this);
    }

    @Override
    public String getResource() {
        return "https://github.com/gpcodervn/Design-Pattern-Tutorial/";
    }

    public String getBestSeller() {
        return "The best Seller of design pattern book";
    }
}
