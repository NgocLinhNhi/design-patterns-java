package com.spring.boot.design.patterns.structural_design_patterns.decorator.handler;

import com.spring.boot.design.patterns.structural_design_patterns.decorator.abstract_decorator_impl.Manager;
import com.spring.boot.design.patterns.structural_design_patterns.decorator.abstract_decorator_impl.TeamLeader;
import com.spring.boot.design.patterns.structural_design_patterns.decorator.interface_component.EmployeeComponent;
import com.spring.boot.design.patterns.structural_design_patterns.decorator.concrete_component.EmployeeComponentImpl;

public class EmployeeDecoratorHandler {
    public static void main(String[] args) {
        System.out.println("NORMAL EMPLOYEE: ");
        EmployeeComponent gpCoder = new EmployeeComponentImpl("GPCoder");
        gpCoder.showBasicInformation();
        gpCoder.doTask();

        System.out.println("\nTEAM LEADER: ");
        EmployeeComponent teamLeader = new TeamLeader(gpCoder);
        teamLeader.showBasicInformation();
        teamLeader.doTask();

        System.out.println("\nMANAGER: ");
        EmployeeComponent manager = new Manager(gpCoder);
        manager.showBasicInformation();
        manager.doTask();

        System.out.println("\nTEAM LEADER AND MANAGER: ");
        EmployeeComponent teamLeaderAndManager = new Manager(teamLeader);
        teamLeaderAndManager.showBasicInformation();
        teamLeaderAndManager.doTask();
    }
}
