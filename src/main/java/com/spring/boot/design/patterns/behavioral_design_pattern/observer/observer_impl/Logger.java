package com.spring.boot.design.patterns.behavioral_design_pattern.observer.observer_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.observer.observer.Observer;
import com.spring.boot.design.patterns.behavioral_design_pattern.observer.request.User;

public class Logger implements Observer {
    @Override
    public void update(User user) {
        System.out.println("Logger: " + user);
    }
}
