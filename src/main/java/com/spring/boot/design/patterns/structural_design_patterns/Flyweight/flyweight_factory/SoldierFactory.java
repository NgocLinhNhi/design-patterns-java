package com.spring.boot.design.patterns.structural_design_patterns.Flyweight.flyweight_factory;

import com.spring.boot.design.patterns.structural_design_patterns.Flyweight.flyweight_impl.Soldier;
import com.spring.boot.design.patterns.structural_design_patterns.Flyweight.flyweight_interfaces.SoliderService;

import java.util.HashMap;
import java.util.Map;

public class SoldierFactory {
    private static final Map<String, SoliderService> soldiers = new HashMap<>();

    private SoldierFactory() {
        throw new IllegalStateException();
    }

    public static synchronized SoliderService createSoldier(String name) {
        //Check đối tượng đã tồn tại thì get ra và tái sử dụng
        SoliderService soldier = soldiers.get(name);
        if (soldier == null) {
            soldier = new Soldier(name);
            soldiers.put(name, soldier);
        }
        return soldier;
    }

    public static synchronized int getTotalOfSoldiers() {
        return soldiers.size();
    }

}
