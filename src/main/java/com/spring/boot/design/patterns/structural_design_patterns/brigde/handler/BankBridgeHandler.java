package com.spring.boot.design.patterns.structural_design_patterns.brigde.handler;

import com.spring.boot.design.patterns.structural_design_patterns.brigde.abstraction.Bank;
import com.spring.boot.design.patterns.structural_design_patterns.brigde.abstraction_impl.TPBank;
import com.spring.boot.design.patterns.structural_design_patterns.brigde.abstraction_impl.VietComBank;
import com.spring.boot.design.patterns.structural_design_patterns.brigde.concrete_implementor.CheckingAccount;
import com.spring.boot.design.patterns.structural_design_patterns.brigde.concrete_implementor.SavingAccount;

public class BankBridgeHandler {
    public static void main(String[] args) {
        Bank vietcomBank = new VietComBank(new CheckingAccount());
        vietcomBank.openAccount();

        Bank tpBank = new TPBank(new SavingAccount());
        tpBank.openAccount();
    }
}
