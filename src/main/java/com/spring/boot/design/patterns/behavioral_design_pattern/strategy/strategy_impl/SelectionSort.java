package com.spring.boot.design.patterns.behavioral_design_pattern.strategy.strategy_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.strategy.strategy_interface.SortStrategy;

import java.util.List;

public class SelectionSort implements SortStrategy {
    @Override
    public <T> void sort(List<T> items) {
        System.out.println("Selection sort");
    }
}
