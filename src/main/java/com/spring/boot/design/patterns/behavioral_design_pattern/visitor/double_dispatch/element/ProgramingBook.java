package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element;

public interface ProgramingBook extends Book {
    String getResource();
}
