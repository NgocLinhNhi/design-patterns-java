package com.spring.boot.design.patterns.creational_design_pattern.prototype.handlers;

import com.spring.boot.design.patterns.creational_design_pattern.prototype.concrete_prototype.ComputerClone;

public class ComputerHandler {
    public static void main(String[] args) {
        ComputerClone computerObj = new ComputerClone(
                "Window 10",
                "Word 2013",
                "BKAV",
                "Chrome v69",
                "SKYPE");
        ComputerClone computerObjClone = computerObj.clone();
        //sau khi clone từ object1 set edit cho computerObjClone các thuộc tính cần đổi
        computerObjClone.setOthers("TEAM_VIEWER, ACTIVE_MQ Client");

        System.out.println(computerObj);
        System.out.println(computerObjClone);
    }
}
