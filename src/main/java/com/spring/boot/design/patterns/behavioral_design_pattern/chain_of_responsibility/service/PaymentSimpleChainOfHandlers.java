package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.handlers.PaymentDepositStep1ValidateRequestHandler;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.handlers.PaymentDepositStep2CheckAccountInfoHandler;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.handlers.PaymentDepositStep3OpenDepositHandler;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces.PaymentChainOfHandlers;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces.PaymentHandler;

import java.util.ArrayList;
import java.util.List;

public class PaymentSimpleChainOfHandlers implements PaymentChainOfHandlers {
    private final List<PaymentHandler> handlers;
    protected final AdminManager adminManager;

    public PaymentSimpleChainOfHandlers(AdminManager adminManager) {
        this.handlers = new ArrayList<>();
        this.adminManager = adminManager;
    }

    void addDepositLeverageHandlers() {
        addHandler(new PaymentDepositStep1ValidateRequestHandler());
        addHandler(new PaymentDepositStep2CheckAccountInfoHandler());
        addHandler(new PaymentDepositStep3OpenDepositHandler());
    }

    //add 1 handler khác cho action nghiệp vụ khác tại đây
    void addDepositExchangeHandlers() {
        addHandler(new PaymentDepositStep1ValidateRequestHandler());
        addHandler(new PaymentDepositStep2CheckAccountInfoHandler());
        addHandler(new PaymentDepositStep3OpenDepositHandler());
    }

    @Override
    public void addHandler(PaymentHandler handler) {
        //XỬ lý add giá trị bean Service truyền vao từ controller cho Chain of repository pattern.
        if (handler instanceof AdminManagerAware)
            ((AdminManagerAware) handler).setAdminManager(adminManager);
        this.handlers.add(handler);
    }

    @Override
    public Object handle(PaymentData data) throws Exception {
        Object result = null;
        for (PaymentHandler handler : handlers)
            result = handler.handle(data);
        return result;
    }
}
