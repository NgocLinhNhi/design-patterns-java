package com.spring.boot.design.patterns.structural_design_patterns.brigde.concrete_implementor;

import com.spring.boot.design.patterns.structural_design_patterns.brigde.implementor.Account;

public class CheckingAccount implements Account {

    //Nghiệp vụ chính thường được giấu trong InterfaceImpl
    @Override
    public void handleAccount() {
        System.out.println(" Checking Account");
    }
}
