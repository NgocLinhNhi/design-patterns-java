package com.spring.boot.design.patterns.behavioral_design_pattern.observer.observer_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.observer.enums.LoginStatus;
import com.spring.boot.design.patterns.behavioral_design_pattern.observer.observer.Observer;
import com.spring.boot.design.patterns.behavioral_design_pattern.observer.request.User;

public class Mailer implements Observer {
    @Override
    public void update(User user) {
        if (user.getStatus() == LoginStatus.EXPIRED) {
            System.out.println("Mailer: User " + user.getEmail() + " is expired. An email was sent!");
        }
    }
}
