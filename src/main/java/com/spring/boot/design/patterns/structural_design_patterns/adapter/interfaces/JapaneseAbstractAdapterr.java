package com.spring.boot.design.patterns.structural_design_patterns.adapter.interfaces;

public interface JapaneseAbstractAdapterr {
    void receive(String words);
}
