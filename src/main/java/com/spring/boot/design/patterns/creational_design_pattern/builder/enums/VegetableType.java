package com.spring.boot.design.patterns.creational_design_pattern.builder.enums;

public enum VegetableType {
    SALAD, CUCUMBER, TOMATO
}
