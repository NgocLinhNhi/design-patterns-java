package com.spring.boot.design.patterns.behavioral_design_pattern.state.context;

import com.spring.boot.design.patterns.behavioral_design_pattern.state.state_interfaces.State;

public class DocumentContext {
    private State state;

    public void setState(State state) {
        this.state = state;
    }

    //Chính là bước set data lại cho PaymentData trong rsbo ( trong chains of repository )
    public void applyState() {
        this.state.handleRequest();
    }
}
