package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Chair;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Table;

/**
 *
 * Đây là Abstract_Factory super của design pattern - tạo ra các method abstract để giấu đi việc khởi tạo cũng như nghiệp vụ xử lý
 *
 */
public abstract class FurnitureAbstractFactory {

    public abstract Chair createChair();

    public abstract Table createTable();
}
