package com.spring.boot.design.patterns.creational_design_pattern.builder.enums;

public enum BreadType {
    SIMPLE, OMELETTE, FRIED_EGG, GRILLED_FISH, PORK, BEEF,
}
