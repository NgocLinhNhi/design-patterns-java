package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.client;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.enums.ServiceType;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service.FiatSimplePaymentService;

import java.math.BigDecimal;

public class ClientTest {
    public static void main(String[] args) throws Exception {
        FiatSimplePaymentService.getInstance().deposit(
                12345,
                BigDecimal.valueOf(5000),
                ServiceType.EXCHANGE);

    }
}