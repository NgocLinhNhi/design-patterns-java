package com.spring.boot.design.patterns.structural_design_patterns.adapter.adapter_process;

import com.spring.boot.design.patterns.structural_design_patterns.adapter.interfaces.JapaneseAbstractAdapterr;

/**
 * Class giao tiếp với interface và adapter lại nghiệp vụ theo ý muốn ở đây
 */
public class JapaneseAdapterr implements JapaneseAbstractAdapterr {

    @Override
    public void receive(String words) {
        System.out.println("After Translated! : ");
        System.out.println(words);
    }
}
