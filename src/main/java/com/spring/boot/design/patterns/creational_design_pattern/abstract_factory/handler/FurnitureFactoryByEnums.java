package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.handler;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.concrete_factory.FlasticFactory;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.concrete_factory.WoodFactory;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.enums.MaterialType;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.FurnitureAbstractFactory;

// Lồng Factory method design pattern
public class FurnitureFactoryByEnums {

    private static FurnitureFactoryByEnums INSTANCE;

    public static FurnitureFactoryByEnums getInstance() {
        if (INSTANCE == null) INSTANCE = new FurnitureFactoryByEnums();
        return INSTANCE;
    }

    private FurnitureFactoryByEnums() {

    }

    public FurnitureAbstractFactory getFactory(MaterialType materialType) {
        switch (materialType) {
            case FLASTIC:
                return new FlasticFactory();
            case WOOD:
                return new WoodFactory();
            default:
                throw new UnsupportedOperationException("This furniture is unsupported ");
        }
    }
}
