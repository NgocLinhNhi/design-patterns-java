package com.spring.boot.design.patterns.behavioral_design_pattern.strategy.client;

import com.spring.boot.design.patterns.behavioral_design_pattern.strategy.context.SortedList;
import com.spring.boot.design.patterns.behavioral_design_pattern.strategy.strategy_impl.MergeSort;
import com.spring.boot.design.patterns.behavioral_design_pattern.strategy.strategy_impl.QuickSort;

public class SortHandler {
    public static void main(String[] args) {

        SortedList sortedList = new SortedList();
        sortedList.add("Java Core");
        sortedList.add("Java Design Pattern");
        sortedList.add("Java Library");
        sortedList.add("Java Framework");

        sortedList.setSortStrategy(new QuickSort());
        sortedList.sort();

        sortedList.setSortStrategy(new MergeSort());
        sortedList.sort();
    }
}
