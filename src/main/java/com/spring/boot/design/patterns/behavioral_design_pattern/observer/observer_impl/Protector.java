package com.spring.boot.design.patterns.behavioral_design_pattern.observer.observer_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.observer.enums.LoginStatus;
import com.spring.boot.design.patterns.behavioral_design_pattern.observer.observer.Observer;
import com.spring.boot.design.patterns.behavioral_design_pattern.observer.request.User;

public class Protector implements Observer {
    @Override
    public void update(User user) {
        if (user.getStatus() == LoginStatus.INVALID) {
            System.out.println("Protector: User " + user.getEmail() + " is invalid. "
                    + "IP " + user.getIp() + " is blocked");
        }
    }
}
