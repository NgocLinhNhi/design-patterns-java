package com.spring.boot.design.patterns.structural_design_patterns.proxy.proxy_virtual_subject;

public interface ProxyService {
    void showImage();
}
