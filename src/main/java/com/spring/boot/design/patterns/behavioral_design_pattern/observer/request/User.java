package com.spring.boot.design.patterns.behavioral_design_pattern.observer.request;

import com.spring.boot.design.patterns.behavioral_design_pattern.observer.enums.LoginStatus;
import lombok.Data;

@Data
public class User {
    private String email;
    private String ip;
    private LoginStatus status;
}
