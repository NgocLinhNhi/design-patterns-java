package com.spring.boot.design.patterns.creational_design_pattern.singleton;

public class TestSingleton {
    public static void main(String[] args) {
        Singleton.getInstance().getCustomer();
    }
}
