package com.spring.boot.design.patterns.behavioral_design_pattern.template_method.template_method_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.template_method.template_abstract_method.PageTemplate;

public class DetailPage extends PageTemplate {
    @Override
    protected void showBody() {
        System.out.println("Content of detail Page");
    }
}
