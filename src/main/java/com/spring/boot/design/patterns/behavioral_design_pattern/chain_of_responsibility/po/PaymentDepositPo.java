package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.po;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class PaymentDepositPo implements Serializable {
    private static final long serialVersionUID = 1234567890123456789L;

    private int customerId;
    private BigDecimal amount;
}