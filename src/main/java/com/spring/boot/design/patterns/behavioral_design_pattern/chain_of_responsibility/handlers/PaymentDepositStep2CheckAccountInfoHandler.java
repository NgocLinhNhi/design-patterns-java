package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.handlers;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.constant.PaymentConstants;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.po.PaymentDepositPo;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service.PaymentData;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service.PaymentDepositAbstractHandler;

public class PaymentDepositStep2CheckAccountInfoHandler extends PaymentDepositAbstractHandler {

    @Override
    public Boolean handle(PaymentData data) {
        long startStep1ProcessTime = data.getStartStep1ProcessTime();
        long startStep2Time = System.currentTimeMillis();
        data.setStep2ProcessTime(startStep2Time);
        long step1Elapsedtime = startStep2Time - startStep1ProcessTime;

        PaymentDepositPo next = data.getStepData(PaymentConstants.DEPOSIT_ENTITY);

        System.out.println("CustomerId :" + next.getCustomerId());
        System.out.println("Amount : " + next.getAmount());
        System.out.println("This is step 2 time process:" + step1Elapsedtime);
        return Boolean.TRUE;
    }
}
