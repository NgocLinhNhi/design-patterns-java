package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces_impl;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Chair;

public class WoodChair implements Chair {
    @Override
    public void create() {
        System.out.println("Create wood chair");
    }
}
