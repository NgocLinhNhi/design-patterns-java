package com.spring.boot.design.patterns.structural_design_patterns.facade.subsystems;

public class SmsService {
    public void sendSMS(String mobilePhone) {
        System.out.println("Sending an mesage to " + mobilePhone);
    }
}
