package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.concrete_factory;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Chair;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Table;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.FurnitureAbstractFactory;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces_impl.WoodChair;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces_impl.WoodTable;

public class WoodFactory extends FurnitureAbstractFactory {
    @Override
    public Chair createChair() {
        return new WoodChair();
    }

    @Override
    public Table createTable() {
        return new WoodTable();
    }
}
