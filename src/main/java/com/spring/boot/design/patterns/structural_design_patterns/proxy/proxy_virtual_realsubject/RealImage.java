package com.spring.boot.design.patterns.structural_design_patterns.proxy.proxy_virtual_realsubject;

import com.spring.boot.design.patterns.structural_design_patterns.proxy.proxy_virtual_subject.ProxyService;

/**
 *
 * Đây là class RealObject xử lý nghiệp vụ chính của proxy pattern
 *
 */
public class RealImage implements ProxyService {
    private String url;

    public RealImage(String url) {
        this.url = url;
        System.out.println("Real Image loaded: " + this.url);
    }

    @Override
    public void showImage() {
        System.out.println("Image showed: " + this.url);
    }
}
