package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.handler;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.FurnitureAbstractFactory;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.concrete_factory.FlasticFactory;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.concrete_factory.WoodFactory;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.enums.MaterialType;

import java.util.HashMap;

public class FurnitureFactoryByHashmap {

    private static FurnitureFactoryByHashmap INSTANCE;
    private final HashMap<MaterialType, FurnitureAbstractFactory> furnitureHandlers = new HashMap<>();

    public static FurnitureFactoryByHashmap getInstance() {
        if (INSTANCE == null) INSTANCE = new FurnitureFactoryByHashmap();
        return INSTANCE;
    }

    private FurnitureFactoryByHashmap() {
        this.furnitureHandlers.put(MaterialType.FLASTIC, new FlasticFactory());
        this.furnitureHandlers.put(MaterialType.WOOD, new WoodFactory());
    }

    public FurnitureAbstractFactory getFactoryByType(MaterialType materialType) {
        return furnitureHandlers.get(materialType);
    }
}
