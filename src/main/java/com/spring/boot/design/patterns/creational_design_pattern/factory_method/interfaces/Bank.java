package com.spring.boot.design.patterns.creational_design_pattern.factory_method.interfaces;

import java.math.BigDecimal;

public interface Bank {

    String getBankName();

    BigDecimal getAmount();
}
