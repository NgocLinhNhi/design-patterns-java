package com.spring.boot.design.patterns.behavioral_design_pattern.state.client;

import com.spring.boot.design.patterns.behavioral_design_pattern.state.context.DocumentContext;
import com.spring.boot.design.patterns.behavioral_design_pattern.state.state_impl.ApprovedState;
import com.spring.boot.design.patterns.behavioral_design_pattern.state.state_impl.NewState;
import com.spring.boot.design.patterns.behavioral_design_pattern.state.state_impl.SubmittedState;

public class DocumentStateHandler {
    public static void main(String[] args) {
        DocumentContext context = new DocumentContext();
        context.setState(new NewState());
        context.applyState();

        context.setState(new SubmittedState());
        context.applyState();

        context.setState(new ApprovedState());
        context.applyState();
    }
}
