package com.spring.boot.design.patterns.behavioral_design_pattern.command.command_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.command.command_interfaces.Command;
import com.spring.boot.design.patterns.behavioral_design_pattern.command.service.Account;

public class OpenAccount implements Command {
    private final Account account;

    public OpenAccount(Account account) {
        this.account = account;
    }

    @Override
    public void execute() {
        account.open();
    }
}
