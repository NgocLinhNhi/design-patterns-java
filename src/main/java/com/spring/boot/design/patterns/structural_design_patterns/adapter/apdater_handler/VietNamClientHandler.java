package com.spring.boot.design.patterns.structural_design_patterns.adapter.apdater_handler;

import com.spring.boot.design.patterns.structural_design_patterns.adapter.adapter_process.JapaneseAdapterr;
import com.spring.boot.design.patterns.structural_design_patterns.adapter.adapter_process.TranslatorAdapterr;

public class VietNamClientHandler {
    public static void main(String[] args) {
        TranslatorAdapterr translatorAdapter = new TranslatorAdapterr(new JapaneseAdapterr());
        translatorAdapter.send("Xin chào");
    }
}
