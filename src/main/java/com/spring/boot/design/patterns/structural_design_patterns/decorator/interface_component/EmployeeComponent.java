package com.spring.boot.design.patterns.structural_design_patterns.decorator.interface_component;

import java.util.Calendar;
import java.util.Date;

public interface EmployeeComponent {

    //Decorator là kiểu hoạt động trên chỉ 1 Object ( wrap lại và mở rộng ) 1 object / interfaces
    // Ở đây là mở rộng = 1 class abstract
    String getName();

    void doTask();

    void join(Date joinDate);

    void terminate(Date terminateDate);

    default void showBasicInformation() {
        System.out.println("-------");
        System.out.println("The basic information of " + getName());

        join(Calendar.getInstance().getTime());

        Calendar terminateDate = Calendar.getInstance();
        terminateDate.add(Calendar.MONTH, 6);
        terminate(terminateDate.getTime());
    }
}
