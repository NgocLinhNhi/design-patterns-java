package com.spring.boot.design.patterns.structural_design_patterns.brigde.concrete_implementor;

import com.spring.boot.design.patterns.structural_design_patterns.brigde.implementor.Account;

public class SavingAccount implements Account {
    @Override
    public void handleAccount() {
        System.out.println("Saving Account");
    }
}
