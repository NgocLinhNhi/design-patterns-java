package com.spring.boot.design.patterns.behavioral_design_pattern.mediator.colleague_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.mediator.colleague.User;
import com.spring.boot.design.patterns.behavioral_design_pattern.mediator.mediator.ChatMediator;

public class UserImpl extends User {
    public UserImpl(ChatMediator mediator, String name) {
        super(mediator, name);
    }

    @Override
    public void send(String msg) {
        System.out.println("---");
        System.out.println(this.name + " is sending the message: " + msg);
        mediator.sendMessage(msg, this);
    }

    @Override
    public void receive(String msg) {
        System.out.println(this.name + " received the message: " + msg);
    }
}
