package com.spring.boot.design.patterns.behavioral_design_pattern.strategy.strategy_interface;

import java.util.List;

public interface SortStrategy {
    <T> void sort(List<T> items);
}
