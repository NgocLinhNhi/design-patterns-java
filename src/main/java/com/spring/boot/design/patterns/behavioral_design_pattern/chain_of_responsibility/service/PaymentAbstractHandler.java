package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces.PaymentHandler;


public abstract class PaymentAbstractHandler
        implements
        PaymentHandler,
        AdminManagerAware {

    protected AdminManager adminManager;

    // ===================== logic ===================

    @Override
    public void setAdminManager(AdminManager adminManager) {
        this.adminManager = adminManager;
    }

}
