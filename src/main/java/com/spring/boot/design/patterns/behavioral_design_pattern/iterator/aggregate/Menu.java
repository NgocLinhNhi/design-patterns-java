package com.spring.boot.design.patterns.behavioral_design_pattern.iterator.aggregate;

import com.spring.boot.design.patterns.behavioral_design_pattern.iterator.iterator.ItemIterator;
import com.spring.boot.design.patterns.behavioral_design_pattern.iterator.iterator_impl.MenuItemIterator;
import com.spring.boot.design.patterns.behavioral_design_pattern.iterator.data.Item;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    public List<Item> menuItems = new ArrayList<>();

    private static Menu INSTANCE;

    public static Menu getInstance() {
        if (INSTANCE == null) INSTANCE = new Menu();
        return INSTANCE;
    }

    public void addItem(Item item) {
        menuItems.add(item);
    }

    public ItemIterator<Item> iterator() {
        return new MenuItemIterator();
    }
}
