package com.spring.boot.design.patterns.behavioral_design_pattern.command.invoke;

import com.spring.boot.design.patterns.behavioral_design_pattern.command.command_interfaces.Command;

public class BankApp {
    private final Command command;

    public BankApp(Command iCommand) {
        this.command = iCommand;
    }

    public void clickOpenAccount() {
        System.out.println("User click open an account");
        command.execute();
    }

    public void clickCloseAccount() {
        System.out.println("User click close an account");
        command.execute();
    }
}
