package com.spring.boot.design.patterns.structural_design_patterns.Flyweight.flyweight_interfaces;

import com.spring.boot.design.patterns.structural_design_patterns.Flyweight.parameters.Context;

public interface SoliderService {
    void promote(Context context);
}
