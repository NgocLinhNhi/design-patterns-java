package com.spring.boot.design.patterns.behavioral_design_pattern.observer.observer;

import com.spring.boot.design.patterns.behavioral_design_pattern.observer.request.User;

public interface Observer {
    void update(User user);
}
