package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service.PaymentData;

public interface PaymentChainOfHandlers {

    void addHandler(PaymentHandler handler);

    //Đây chính là Strategy pattern đi kèm chain of repository
    Object handle(PaymentData data) throws Exception;

}
