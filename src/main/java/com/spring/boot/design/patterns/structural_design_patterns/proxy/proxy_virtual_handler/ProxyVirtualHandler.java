package com.spring.boot.design.patterns.structural_design_patterns.proxy.proxy_virtual_handler;

import com.spring.boot.design.patterns.structural_design_patterns.proxy.proxy_virtual_proxy.ProxyImage;

public class ProxyVirtualHandler {
    public static void main(String[] args) {
        System.out.println("Init proxy image: ");
        ProxyImage proxyImage = ProxyImage.getInstance("https://gpcoder.com/favicon.ico");

        System.out.println("---");
        System.out.println("Call real data 1st: ");
        proxyImage.showImage();

        System.out.println("---");
        System.out.println("Call real data 2nd: ");
        proxyImage.showImage();
    }
}
