package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service;

public interface AdminManagerAware {

    void setAdminManager(AdminManager adminManager);

}
