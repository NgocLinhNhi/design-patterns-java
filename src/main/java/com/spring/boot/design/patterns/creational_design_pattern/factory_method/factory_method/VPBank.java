package com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory_method;

import com.spring.boot.design.patterns.creational_design_pattern.factory_method.interfaces.Bank;

import java.math.BigDecimal;

public class VPBank implements Bank {
    @Override
    public String getBankName() {
        return "VPBank";
    }

    @Override
    public BigDecimal getAmount() {
        BigDecimal money = new BigDecimal("50000");
        return money;
    }
}
