package com.spring.boot.design.patterns.behavioral_design_pattern.mediator.mediator_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.mediator.colleague.User;
import com.spring.boot.design.patterns.behavioral_design_pattern.mediator.mediator.ChatMediator;

import java.util.ArrayList;
import java.util.List;

public class ChatMediatorImpl implements ChatMediator {

    public ChatMediatorImpl(String groupName) {
        System.out.println(groupName + " group already created");
    }

    private final List<User> users = new ArrayList<>();

    @Override
    public void addUser(User user) {
        System.out.println(user.name + " joined this group");
        this.users.add(user);
    }

    @Override
    public void sendMessage(String msg, User user) {
        for (User u : this.users) {
            if (!u.equals(user)) {
                u.receive(msg);
            }
        }
    }
}
