package com.spring.boot.design.patterns.creational_design_pattern.prototype.concrete_prototype;

public class ComputerClone implements Cloneable {

    private String os;
    private String office;
    private String antivirus;
    private String browser;
    private String others;

    public ComputerClone(String os, String office, String antivirus, String browser, String other) {
        super();
        this.os = os;
        this.office = office;
        this.antivirus = antivirus;
        this.browser = browser;
        this.others = other;
    }

    @Override
    public ComputerClone clone() {
        try {
            return (ComputerClone) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "ComputerClone [os=" + os + ", office=" + office + ", antivirus=" + antivirus + ", browser=" + browser
                + ", others=" + others + "]";
    }

    public void setOthers(String others) {
        this.others = others;
    }
}
