package com.spring.boot.design.patterns.behavioral_design_pattern.command.handler;

import com.spring.boot.design.patterns.behavioral_design_pattern.command.command_impl.CloseAccount;
import com.spring.boot.design.patterns.behavioral_design_pattern.command.command_impl.OpenAccount;
import com.spring.boot.design.patterns.behavioral_design_pattern.command.command_interfaces.Command;
import com.spring.boot.design.patterns.behavioral_design_pattern.command.invoke.BankApp;
import com.spring.boot.design.patterns.behavioral_design_pattern.command.service.Account;

public class BankAppHandler {
    public static void main(String[] args) {

        Command open = new OpenAccount(Account.getInstance());
        Command close = new CloseAccount(Account.getInstance());

        BankApp openAccount = new BankApp(open);
        BankApp closeAccount = new BankApp(close);

        openAccount.clickOpenAccount();
        closeAccount.clickCloseAccount();
    }
}
