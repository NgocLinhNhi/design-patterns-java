package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element;

import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.visitor_interfaces.Visitor;

public interface Book {
    void accept(Visitor v);

    void getResource(Visitor v);
}
