package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.enums;

public enum MaterialType {
    FLASTIC, WOOD;
}
