package com.spring.boot.design.patterns.behavioral_design_pattern.template_method.template_abstract_method;

public abstract class PageTemplate {
    //chính là request Handle của Rsbo
    protected void showHeader() {
        System.out.println("< header / > ");
    }

    protected void showNavigation() {
        System.out.println("This is navigation bar ");
    }

    protected void showFooter() {
        System.out.println("  < footer / > ");
    }

    protected abstract void showBody();

    public final void showPage() {
        showHeader();
        showNavigation();
        showBody();
        showFooter();
    }
}
