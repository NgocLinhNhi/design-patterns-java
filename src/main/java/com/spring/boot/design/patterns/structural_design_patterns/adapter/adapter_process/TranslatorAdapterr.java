package com.spring.boot.design.patterns.structural_design_patterns.adapter.adapter_process;

import com.spring.boot.design.patterns.structural_design_patterns.adapter.interfaces.JapaneseAbstractAdapterr;
import com.spring.boot.design.patterns.structural_design_patterns.adapter.interfaces.VietnameseAbstractAdapterr;

public class TranslatorAdapterr implements VietnameseAbstractAdapterr {

    private final JapaneseAbstractAdapterr adapter;

    public TranslatorAdapterr(JapaneseAbstractAdapterr adapter) {
        this.adapter = adapter;
    }

    @Override
    public void send(String vietnameseWords) {
        System.out.println("Before translate: " + vietnameseWords);
        String japaneseWords = translate(vietnameseWords);
        adapter.receive(japaneseWords);
    }

    private String translate(String vietnameseWords) {
        if (vietnameseWords.equals("Xin chào")) {
            return "こんにちは";
        } else {
            return "いいえ";
        }
    }
}
