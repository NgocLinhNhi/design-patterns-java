package com.spring.boot.design.patterns.behavioral_design_pattern.strategy.context;

import com.spring.boot.design.patterns.behavioral_design_pattern.strategy.strategy_interface.SortStrategy;

import java.util.ArrayList;
import java.util.List;

public class SortedList {

    //Chính là bước add các steps vào handler của chains of repository
    private SortStrategy strategy;
    private final List<String> items = new ArrayList<>();

    public void setSortStrategy(SortStrategy strategy) {
        this.strategy = strategy;
    }

    public void add(String name) {
        items.add(name);
    }

    //Đây chính là method handle
    public void sort() {
        strategy.sort(items);
    }
}
