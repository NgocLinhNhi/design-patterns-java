package com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory;

import com.spring.boot.design.patterns.creational_design_pattern.factory_method.enums.BankTypeEnum;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory_method.VPBank;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory_method.VietInBank;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.interfaces.Bank;

public class BankFactoryByEnum {

    public static BankFactoryByEnum INSTANCE;

    public static BankFactoryByEnum getInstance() {
        if (INSTANCE == null) INSTANCE = new BankFactoryByEnum();
        return INSTANCE;
    }

    private BankFactoryByEnum() {
    }

    public Bank getBank(BankTypeEnum bankType) {
        switch (bankType) {
            case VPBANK:
                return new VPBank();
            case VIETINBANK:
                return new VietInBank();
            default:
                throw new IllegalArgumentException("This bank type is unsupported");
        }
    }
}
