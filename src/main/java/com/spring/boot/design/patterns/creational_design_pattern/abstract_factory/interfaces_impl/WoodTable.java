package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces_impl;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Table;

public class WoodTable implements Table {
    @Override
    public void create() {
        System.out.println("Create wood table");
    }
}
