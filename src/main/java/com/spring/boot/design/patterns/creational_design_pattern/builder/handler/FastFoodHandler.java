package com.spring.boot.design.patterns.creational_design_pattern.builder.handler;

import com.spring.boot.design.patterns.creational_design_pattern.builder.concrete_builder.FastFoodOrderBuilder;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.BreadType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.OrderType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.SauceType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.enums.VegetableType;
import com.spring.boot.design.patterns.creational_design_pattern.builder.product.Order;
import com.spring.boot.design.patterns.creational_design_pattern.builder.request.ProductRequest;

public class FastFoodHandler {
    public static void main(String[] args) {
        //không muốn set thằng nào thì ko set ở đây là đc tiện lợi cho build request
        Order order = new FastFoodOrderBuilder()
                .setOrderType(OrderType.ON_SITE)
                .setOrderBread(BreadType.OMELETTE)
                .setOrderSauce(SauceType.SOY_SAUCE)
                .setOrderVegetable(VegetableType.SALAD)
                .setProduct(buildProduct())
                .build();
        System.out.println(order);
        System.out.println("Get từng request riêng lẽ: " + order.getBreadType());
    }

    public static ProductRequest buildProduct() {
        return new ProductRequest("NINH",31,"HA NOI");
    }
}
