package com.spring.boot.design.patterns.structural_design_patterns.proxy.proxy_virtual_proxy;

import com.spring.boot.design.patterns.structural_design_patterns.proxy.proxy_virtual_realsubject.RealImage;
import com.spring.boot.design.patterns.structural_design_patterns.proxy.proxy_virtual_subject.ProxyService;

/**
 *
 * Đây là class proxy khởi tạo = singleton sẽ gọi đến realObject lấy xử lý method
 *
 */
public class ProxyImage implements ProxyService {
    private ProxyService proxyService;
    private String url;

    private static ProxyImage INSTANCE;

    public static ProxyImage getInstance(String url) {
        if (INSTANCE == null) INSTANCE = new ProxyImage(url);
        return INSTANCE;
    }

    public ProxyImage(String url) {
        this.url = url;
        System.out.println("Image unloaded: " + this.url);
    }


    /**
     *
     * Gọi đến realObject lấy xử lý nghiệp vụ - RealImage
     *
     */
    @Override
    public void showImage() {
        if (proxyService == null) {
            proxyService = new RealImage(this.url);
        } else {
            System.out.println("Image already existed: " + this.url);
        }
        proxyService.showImage();
    }
}
