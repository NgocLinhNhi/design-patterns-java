package com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.handler;

import com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.context.InterpreterEngineContext;
import com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.expression.Expression;
import com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.expression_impl.AddExpression;
import com.spring.boot.design.patterns.behavioral_design_pattern.Interpreter.expression_impl.SubtractExpression;

public class CalculatorClient {
    public static void main(String args[]) {
        System.out.println("20 cộng 8 = " + interpret("20 cộng 8"));
        System.out.println("10 trừ 4 = " + interpret("10 trừ 4"));
    }

    private static int interpret(String input) {
        Expression exp;
        if (input.contains("cộng")) {
            exp = new AddExpression(input);
        } else if (input.contains("trừ")) {
            exp = new SubtractExpression(input);
        } else {
            throw new UnsupportedOperationException();
        }
        return exp.interpret(new InterpreterEngineContext());
    }
}
