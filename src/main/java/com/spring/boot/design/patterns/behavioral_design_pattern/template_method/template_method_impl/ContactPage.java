package com.spring.boot.design.patterns.behavioral_design_pattern.template_method.template_method_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.template_method.template_abstract_method.PageTemplate;

public class ContactPage extends PageTemplate {

    @Override
    protected void showNavigation() {
        // Just do nothing
        // Because we don't want to show navigation bar on contact page
    }

    @Override
    protected void showBody() {
        System.out.println("Content of contact page");
    }
}
