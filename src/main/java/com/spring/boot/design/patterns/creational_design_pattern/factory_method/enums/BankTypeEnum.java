package com.spring.boot.design.patterns.creational_design_pattern.factory_method.enums;

public enum BankTypeEnum {
    VIETINBANK, VPBANK;
}
