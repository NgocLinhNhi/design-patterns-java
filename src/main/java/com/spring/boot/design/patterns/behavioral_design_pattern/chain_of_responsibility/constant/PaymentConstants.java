package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.constant;

public final class PaymentConstants {

    public static final String DEPOSIT_ENTITY = "DepositEntity";

}
