package com.spring.boot.design.patterns.behavioral_design_pattern.strategy.strategy_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.strategy.strategy_interface.SortStrategy;

import java.util.List;

public class MergeSort implements SortStrategy {
    @Override
    public <T> void sort(List<T> items) {
        System.out.println("This is Merge sort");
        for (T obj : items) {
            System.out.println(obj);
        }
    }
}
