package com.spring.boot.design.patterns.creational_design_pattern.builder.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ProductRequest implements Serializable {
    private String prouctName;
    private int age;
    private String address;

    public ProductRequest(String prouctName, int age, String address) {
        this.prouctName = prouctName;
        this.age = age;
        this.address = address;
    }
}
