package com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory;

import com.spring.boot.design.patterns.creational_design_pattern.factory_method.enums.BankTypeEnum;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory_method.VPBank;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.factory_method.VietInBank;
import com.spring.boot.design.patterns.creational_design_pattern.factory_method.interfaces.Bank;

import java.util.HashMap;
import java.util.Map;

public class BankFactoryByHashMap {

    protected final Map<BankTypeEnum, Bank> handlers;

    private static BankFactoryByHashMap INSTANCE;

    public static BankFactoryByHashMap getInstance() {
        if (INSTANCE == null) INSTANCE = new BankFactoryByHashMap();
        return INSTANCE;
    }

    private BankFactoryByHashMap() {
        this.handlers = new HashMap<>();
        this.handlers.put(BankTypeEnum.VIETINBANK, new VietInBank());
        this.handlers.put(BankTypeEnum.VPBANK, new VPBank());
    }

    public Bank getBank(BankTypeEnum bankType) {
        return handlers.get(bankType);
    }
}
