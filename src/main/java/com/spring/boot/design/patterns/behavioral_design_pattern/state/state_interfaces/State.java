package com.spring.boot.design.patterns.behavioral_design_pattern.state.state_interfaces;

public interface State {
    void handleRequest();
}
