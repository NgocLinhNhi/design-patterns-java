package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.client;

import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.data.Book;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.data_extends.ProgramingBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.interfaces_impl.Developer;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.single_dispatch_interfaces.Customer;

public class SingleDispatchHandler {
    public static void main(String[] args) {
        Book book = new ProgramingBook();
        Customer gpcoder = new Developer();
        gpcoder.buy(book);

        ProgramingBook programingBook = new ProgramingBook();
        gpcoder.buy(programingBook);
    }
}
