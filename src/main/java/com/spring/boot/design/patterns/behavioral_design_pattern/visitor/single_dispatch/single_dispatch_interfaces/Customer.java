package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.single_dispatch_interfaces;


import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.data.Book;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.data_extends.BusinessBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.single_dispatch.data_extends.ProgramingBook;

public interface Customer {
    void buy(Book book);

    void buy(ProgramingBook book);

    void buy(BusinessBook book);
}
