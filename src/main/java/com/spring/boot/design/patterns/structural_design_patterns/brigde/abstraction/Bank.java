package com.spring.boot.design.patterns.structural_design_patterns.brigde.abstraction;

import com.spring.boot.design.patterns.structural_design_patterns.brigde.implementor.Account;

public abstract class Bank {
    //Account này sẽ được dùng trong các abstractionImpl
    // -> gọi đến method của InterfaceImpl để gọi ra xử lý nghiệp vụ chính
    protected Account account;
    public Bank(Account account) {
        this.account = account;
    }
    public abstract void openAccount();
}
