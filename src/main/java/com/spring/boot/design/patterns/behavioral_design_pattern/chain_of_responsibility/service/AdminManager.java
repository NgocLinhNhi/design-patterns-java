package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service;

public class AdminManager {

    private static AdminManager INSTANCE;

    public static AdminManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AdminManager();
        }
        return INSTANCE;
    }

    public void getAdmin() {
        System.out.println("This is get Admin method from Services/component of Spring Bean");
    }
}
