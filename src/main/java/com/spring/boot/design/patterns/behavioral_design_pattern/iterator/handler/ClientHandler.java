package com.spring.boot.design.patterns.behavioral_design_pattern.iterator.handler;

import com.spring.boot.design.patterns.behavioral_design_pattern.iterator.aggregate.Menu;
import com.spring.boot.design.patterns.behavioral_design_pattern.iterator.iterator.ItemIterator;
import com.spring.boot.design.patterns.behavioral_design_pattern.iterator.data.Item;

public class ClientHandler {
    public static void main(String[] args) {

        Menu instance = Menu.getInstance();

        instance.addItem(new Item("Home", "/home"));
        instance.addItem(new Item("Java", "/java"));
        instance.addItem(new Item("Spring Boot", "/spring-boot"));

        ItemIterator<Item> iterator = instance.iterator();
        while (iterator.hasNext()) {
            Item item = iterator.next();
            System.out.println(item);
        }
    }
}
