package com.spring.boot.design.patterns.behavioral_design_pattern.state.state_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.state.state_interfaces.State;

public class ApprovedState implements State {
    @Override
    public void handleRequest() {
        System.out.println("Approved");
    }
}
