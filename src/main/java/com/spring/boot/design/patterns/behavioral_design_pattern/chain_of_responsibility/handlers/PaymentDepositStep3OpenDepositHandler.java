package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.handlers;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces.PaymentHandler;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service.PaymentData;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service.PaymentDepositAbstractHandler;

public class PaymentDepositStep3OpenDepositHandler extends PaymentDepositAbstractHandler {

    @Override
    public Boolean handle(PaymentData data) {
        long step2ProcessTime = data.getStep2ProcessTime();
        long step3ProcessTime = System.currentTimeMillis();
        long step3ElapsedTime = step3ProcessTime - step2ProcessTime;

        System.out.println("This is step 3 - end time handle process deposit :" + step3ElapsedTime);
        return Boolean.TRUE;
    }

}
