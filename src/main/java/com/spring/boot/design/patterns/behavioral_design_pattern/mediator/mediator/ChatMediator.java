package com.spring.boot.design.patterns.behavioral_design_pattern.mediator.mediator;

import com.spring.boot.design.patterns.behavioral_design_pattern.mediator.colleague.User;

public interface ChatMediator {
    void sendMessage(String msg, User user);

    void addUser(User user);
}
