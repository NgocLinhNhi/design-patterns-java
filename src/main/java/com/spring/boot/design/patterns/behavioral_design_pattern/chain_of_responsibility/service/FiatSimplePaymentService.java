package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.enums.ServiceType;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces.FiatDepositService;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces.PaymentChainOfHandlers;

import java.math.BigDecimal;

public class FiatSimplePaymentService implements FiatDepositService {

    private static FiatSimplePaymentService INSTANCE;

    public static FiatSimplePaymentService getInstance() {
        if (INSTANCE == null) INSTANCE = new FiatSimplePaymentService();
        return INSTANCE;
    }

    @Override
    public void deposit(int customerId,
                        BigDecimal amount,
                        ServiceType serviceType) throws Exception {
        PaymentService paymentService = PaymentService.getInstance();
        PaymentChainOfHandlers paymentChainOfHandlers = paymentService.newDepositChainOfHandlers(
                serviceType, AdminManager.getInstance());
        PaymentData paymentData = paymentService.newPaymentData(customerId, amount);
        paymentChainOfHandlers.handle(paymentData);
    }


}
