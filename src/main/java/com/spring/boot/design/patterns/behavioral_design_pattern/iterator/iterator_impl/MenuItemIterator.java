package com.spring.boot.design.patterns.behavioral_design_pattern.iterator.iterator_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.iterator.aggregate.Menu;
import com.spring.boot.design.patterns.behavioral_design_pattern.iterator.iterator.ItemIterator;
import com.spring.boot.design.patterns.behavioral_design_pattern.iterator.data.Item;

public class MenuItemIterator implements ItemIterator<Item> {
    private int currentIndex = 0;

    @Override
    public boolean hasNext() {
        return currentIndex < Menu.getInstance().menuItems.size();
    }

    @Override
    public Item next() {
        return Menu.getInstance().menuItems.get(currentIndex++);
    }
}
