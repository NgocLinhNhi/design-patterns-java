package com.spring.boot.design.patterns.behavioral_design_pattern.observer.enums;

public enum LoginStatus {
    SUCCESS, FAILURE, INVALID, EXPIRED;
}
