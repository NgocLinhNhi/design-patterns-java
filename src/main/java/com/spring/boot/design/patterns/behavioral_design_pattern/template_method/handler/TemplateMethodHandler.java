package com.spring.boot.design.patterns.behavioral_design_pattern.template_method.handler;

import com.spring.boot.design.patterns.behavioral_design_pattern.template_method.template_abstract_method.PageTemplate;
import com.spring.boot.design.patterns.behavioral_design_pattern.template_method.template_method_impl.ContactPage;
import com.spring.boot.design.patterns.behavioral_design_pattern.template_method.template_method_impl.DetailPage;
import com.spring.boot.design.patterns.behavioral_design_pattern.template_method.template_method_impl.HomePage;

public class TemplateMethodHandler {
    public static void main(String[] args) {

        PageTemplate homePage = new HomePage();
        homePage.showPage();

        System.out.println();
        PageTemplate detailPage = new DetailPage();
        detailPage.showPage();

        System.out.println();
        PageTemplate contactPage = new ContactPage();
        contactPage.showPage();
    }
}
