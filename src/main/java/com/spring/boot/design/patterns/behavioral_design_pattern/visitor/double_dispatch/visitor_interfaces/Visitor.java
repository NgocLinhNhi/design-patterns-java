package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.visitor_interfaces;

import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl.BusinessBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl.DesignPatternBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl.JavaCoreBook;

public interface Visitor {
    void visit(BusinessBook book);

    void visit(DesignPatternBook book);

    void visit(JavaCoreBook book);

    void getResource(BusinessBook book);

    void getResource(DesignPatternBook book);

    void getResource(JavaCoreBook book);
}
