package com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.service;

import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.enums.ServiceType;
import com.spring.boot.design.patterns.behavioral_design_pattern.chain_of_responsibility.interfaces.PaymentChainOfHandlers;

import java.math.BigDecimal;

class PaymentService {

    private static PaymentService INSTANCE;

    public static PaymentService getInstance() {
        if (INSTANCE == null) INSTANCE = new PaymentService();
        return INSTANCE;
    }

    PaymentChainOfHandlers newDepositChainOfHandlers(ServiceType serviceType,
                                                     AdminManager adminManager) {
        PaymentSimpleChainOfHandlers answer = new PaymentSimpleChainOfHandlers(adminManager);
        if (serviceType == ServiceType.EXCHANGE)
            answer.addDepositLeverageHandlers();
        else
            answer.addDepositExchangeHandlers();
        return answer;
    }

    PaymentData newPaymentData(int customerId, BigDecimal amount) {
        return new PaymentData(customerId, amount);
    }
}
