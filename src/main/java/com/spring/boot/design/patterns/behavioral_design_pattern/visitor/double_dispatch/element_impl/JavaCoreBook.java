package com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element_impl;

import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.element.ProgramingBook;
import com.spring.boot.design.patterns.behavioral_design_pattern.visitor.double_dispatch.visitor_interfaces.Visitor;

public class JavaCoreBook implements ProgramingBook {

    @Override
    public void accept(Visitor v) {
        v.visit(this);
    }

    @Override
    public void getResource(Visitor v) {
        v.getResource(this);
    }

    @Override
    public String getResource() {
        return "https://github.com/gpcodervn/Java-Tutorial/";
    }

    public String getFavouriteBook() {
        return "The most favourite book of java core";
    }
}
