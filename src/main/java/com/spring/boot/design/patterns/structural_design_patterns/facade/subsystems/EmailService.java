package com.spring.boot.design.patterns.structural_design_patterns.facade.subsystems;

public class EmailService {

    public void sendMail(String mailTo) {
        System.out.println("Sending an email to " + mailTo);
    }
}
