package com.spring.boot.design.patterns.structural_design_patterns.composite.base_component;

public interface FileComponent {
    void showProperty();

    long totalSize();
}
