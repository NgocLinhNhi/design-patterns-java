package com.spring.boot.design.patterns.structural_design_patterns.brigde.abstraction_impl;

import com.spring.boot.design.patterns.structural_design_patterns.brigde.abstraction.Bank;
import com.spring.boot.design.patterns.structural_design_patterns.brigde.implementor.Account;

public class TPBank extends Bank {

    public TPBank(Account account) {
        super(account);
    }

    @Override
    public void openAccount() {
        System.out.print("Open your account at TPBank is a ");
        account.handleAccount();
    }
}
