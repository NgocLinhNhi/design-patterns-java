package com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.handler;

import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.abstract_factory.FurnitureAbstractFactory;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.enums.MaterialType;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Chair;
import com.spring.boot.design.patterns.creational_design_pattern.abstract_factory.interfaces.Table;

public class FurnitureAbstractFactoryHandlerByHasMap {
    public static void main(String[] args) {
        FurnitureFactoryByHashmap instance = FurnitureFactoryByHashmap.getInstance();

        //Abstract Factory là kết hợp của nhiều factory con
        //Get Factory Con
        FurnitureAbstractFactory plasticFactory = instance.getFactoryByType(MaterialType.FLASTIC);
        //Get Interface - gọi ra xử lý business
        Chair plasticChair = plasticFactory.createChair();
        plasticChair.create();
        Table plasticTable = plasticFactory.createTable();
        plasticTable.create();

        FurnitureAbstractFactory woodFactory = instance.getFactoryByType(MaterialType.WOOD);
        Chair woodChair = woodFactory.createChair();
        woodChair.create();
        Table woodTable = woodFactory.createTable();
        woodTable.create();
    }
}
